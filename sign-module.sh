#!/bin/bash
i=$1
o="${i/-unsigned/-signed}"

echo "Removing any previously signed certs"
rm $o 2> /dev/null

echo "Signing $i using the ignition signing tool"
echo "Saving to $o"
java -jar module-signer.jar \
     -keystore=./certificate.pfx \
     -keystore-pwd=nopass -alias=1 \
     -alias-pwd=nopass -chain=./certificate.p7b \
     -module-in=$i \
     -module-out=$o

