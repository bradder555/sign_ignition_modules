#!/bin/bash
echo "Prevent Msys from 'helpfully' interpreting paths"
export MSYS_NO_PATHCONV=0;

echo "Remove all previous certs"
rm ./certificate.* 2> /dev/null

echo "Creates a standard PEM encoded private / public key pair"
openssl req -new -newkey rsa:4096 -passout pass:nopass \
        -days 3650 -x509 \
        -subj "/C=AU/ST=WA/L=Perth/O=BHP/CN=TROC" \
        -keyout certificate.key -out certificate.crt &> /dev/null

echo "Creates the PKCS#7 public key container"
openssl crl2pkcs7 -nocrl -certfile certificate.crt \
        -out certificate.p7b &> /dev/null

echo "Creates the PKCS#12 Certificate chain of trust"
openssl pkcs12 -export -inkey certificate.key \
        -passout pass:nopass -passin pass:nopass \
        -in certificate.crt -out certificate.pfx &> /dev/null
